//
//  ViewController.swift
//  03. Random Color
//
//  Created by x0000ff on 12/07/15.
//  Copyright (c) 2015 x0000ff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var randomColoredView: UIView!
 
    @IBAction func generatedRandomColor(sender: AnyObject) {
        
        let randomRed   = CGFloat(arc4random() % 255) / 255.0
        let randomGreen = CGFloat(arc4random() % 255) / 255.0
        let randomBlue  = CGFloat(arc4random() % 255) / 255.0
        
        let randomColor = UIColor(red: randomRed, green: randomGreen, blue: randomGreen, alpha: 1)
        
        randomColoredView.backgroundColor = randomColor
    }
    
}

